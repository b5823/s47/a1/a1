console.log(document);

const txtFirstName = document.querySelector("#txt-first-name");

console.log(txtFirstName);

const txtLastName = document.querySelector("#txt-last-name");

console.log(txtLastName);

let spanFullName = document.querySelector("#span-full-name");

// >>>> SOLUTION 1
// txtFirstName.addEventListener && txtLastName.addEventListener("keyup", (e) => {

// 	(spanFullName.innerHTML = `${txtFirstName.value}` + `${txtLastName.value}`)

// });

// >>> SOLUTION 2
// ---> This worked however you have to press enter (or any key I think) for the last name to show up.
// txtFirstName.addEventListener ("input", (event) => {

// 	console.log(event);

// 	spanFullName.innerHTML = txtFirstName.value
// });

// txtLastName.addEventListener("change", (event) => {

// 	console.log(txtLastName);

// 	spanFullName.innerHTML += txtLastName.value
// });

// >>> SOLUTION 3
txtFirstName.addEventListener("keyup", updateValue);
txtLastName.addEventListener("keyup", updateValue);

function updateValue (event) {
	console.log(event)
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
};